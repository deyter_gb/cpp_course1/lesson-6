//
// Created by aard on 23.09.2021.
//
#include <iostream>

#ifndef LESSON6__ARRAYHELPERS_H_
#define LESSON6__ARRAYHELPERS_H_

/**
 * Create and fill array with pow of two.
 *
 * @param SIZE size_t
 * Size of array to initialize.
 * @return
 * Intitalized array.
 */
int *InitArrayWithPowOfTwo(const size_t SIZE);

/**
 * Fill matrix with random numbers.
 *
 * @param SIZE size_t
 * Size of matrix.
 * @return
 * Filled matrix.
 */
int **InitMatrixWithRand(const size_t SIZE);

/**
 * Fill array with random chars.
 *
 * @param SIZE size_t
 * Size of array.
 * @return
 * Filled array.
 */
char *InitArrayWithChars(const size_t SIZE);

/**
 * Print array to console.
 *
 * @param arr int[]
 * Array to print.
 * @param size size_t
 * Size of array.
 * @return
 * Result of print.
 */
bool PrintArray(int arr[], size_t size);

/**
 * Print matrix to console.
 *
 * @param arr int[]
 * Matrix to print.
 * @param size size_t
 * Size of array.
 * @return
 * Result of print.
 */
bool PrintMatrix(int **arr, size_t size);

/**
 * Free memory.
 *
 * @param arr int**
 * Matrix to delete.
 * @param size size_t
 * Size of matrix.
 */
void DeleteMatrix(int **arr, size_t size);

#endif //LESSON6__ARRAYHELPERS_H_
