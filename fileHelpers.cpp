//
// Created by aard on 23.09.2021.
//
#include <iostream>
#include <fstream>
#include "arrayHelpers.h"

const size_t STRING_MIN_LENGTH = 50;
const size_t STRING_MAX_LENGTH = 100;

void PrintCharsToFile(const std::string *FILENAME, const char OUTPUT[], const size_t OUT_SIZE) {
    std::ofstream fout(*FILENAME);
    for (size_t i = 0; i < OUT_SIZE; i++) {
        fout << OUTPUT[i];
    }
    fout.close();
}

int ReadFromFileAndGetDelta(const std::string &FILENAME1, size_t filesize1, char *input, int i) {
    std::ifstream fin1(FILENAME1);
    if (fin1.is_open()) {
        for (; i < filesize1; i++) {
            fin1 >> &input[i];
        }
        fin1.close();
    }
    return i;
}

void InitRandomCharsAndSaveToFile(const std::string *FILENAME1) {
    const size_t SIZE1 = STRING_MIN_LENGTH + (rand() % (STRING_MAX_LENGTH - STRING_MIN_LENGTH));
    char *output = InitArrayWithChars(SIZE1);
    PrintCharsToFile(FILENAME1, output, SIZE1);
    delete[] output;
}

std::string *ReadStringFromFile(const std::string &FILENAME) {
    auto *readed_string = new std::string;
    std::ifstream fin(FILENAME);
    while (!fin.eof()) {
        getline(fin, *readed_string);
    }
    fin.close();
    return readed_string;
}
