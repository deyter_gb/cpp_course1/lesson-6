//
// Created by aard on 23.09.2021.
//

#ifndef LESSON6__FILEHELPERS_H_
#define LESSON6__FILEHELPERS_H_

/**
 * Helper function to put chars from array to file.
 *
 * @param FILENAME std::string
 * Filename to write.
 * @param OUTPUT char[]
 * Char array to output.
 * @param OUT_SIZE size_t
 * Size of char array.
 */
void PrintCharsToFile(const std::string *FILENAME, const char OUTPUT[], const size_t OUT_SIZE);

/**
 * Read string from file and return size.
 *
 * @param FILENAME1 std::string
 * Filename to read.
 * @param filesize1 size_t
 * Filesize.
 * @param input char*
 * Chars to read in it from file.
 * @param i int
 * Delta to start.
 * @return
 * Changed delta.
 */
int ReadFromFileAndGetDelta(const std::string &FILENAME1, size_t filesize1, char *input, int i);

/**
 * Save randoms chars to file.
 *
 * @param FILENAME1 std::string
 * Filename to save.
 */
void InitRandomCharsAndSaveToFile(const std::string *FILENAME1);

/**
 * Read string from file.
 *
 * @param FILENAME
 * Filename to read.
 * @return
 * String readed from file.
 */
std::string *ReadStringFromFile(const std::string &FILENAME);

#endif //LESSON6__FILEHELPERS_H_
