#include <iostream>
#include <fstream>
#include <filesystem>
#include "arrayHelpers.h"
#include "fileHelpers.h"

/* #region constants */
const std::string TASK_SEPARATOR = "========================================================";

const size_t MIN_LENGTH = 1;
const size_t MAX_LENGTH = 31;

const size_t MATRIX_SIZE = 4;
/* #endregion */

/* #region Helper functions */
/**
 * Print separator between tasks.
 */
void inline PrintSeparator() {
  std::cout << TASK_SEPARATOR << std::endl;
}

bool CheckForSubstring(const std::string *READED_STRING, const char *STRING_TO_FOUND) {
  if ((*READED_STRING).find(STRING_TO_FOUND) != std::string::npos) {
    std::cout << "Found" << std::endl;
    return true;
  } else {
    std::cout << "Not found" << std::endl;
    return false;
  }
}
/* #endregion */

/* #region Task functions */
void Task1() {
  size_t size = 0;
  std::cout << "Enter size of array from " << MIN_LENGTH << " to " << MAX_LENGTH <<":";
  while (size < MIN_LENGTH || size > MAX_LENGTH) {
    std::cin >> size;
  }
  int *i_d_arr = InitArrayWithPowOfTwo(size);
  PrintArray(i_d_arr, size);
  delete[] i_d_arr;
}

void Task2() {
  int **i_matrix = InitMatrixWithRand(MATRIX_SIZE);
  PrintMatrix(i_matrix, MATRIX_SIZE);
  DeleteMatrix(i_matrix, MATRIX_SIZE);
}

void Task3(std::string *filename1, std::string *filename2) {
  std::cout << "Enter first filename:";
  std::cin >> *filename1;
  std::cout << "Enter second filename:";
  std::cin >> *filename2;
  InitRandomCharsAndSaveToFile(filename1);
  InitRandomCharsAndSaveToFile(filename2);
}

void Task4(const std::string *FILENAME1, const std::string *FILENAME2) {
  std::string filename3;
  std::cout << "Enter third filename:";
  std::cin >> filename3;
  std::ofstream fout(filename3);
  size_t filesize1 = std::filesystem::file_size(*FILENAME1) / sizeof(char);
  size_t filesize2 = std::filesystem::file_size(*FILENAME2) / sizeof(char);
  char *input = new char[filesize1 + filesize2];
  int i = ReadFromFileAndGetDelta(*FILENAME1, filesize1, input, 0);
  ReadFromFileAndGetDelta(*FILENAME2, filesize1 + filesize2, input, i);
  PrintCharsToFile(&filename3, input, filesize1 + filesize2);
  delete[] input;
}

void Task5() {
  std::string filename;
  std::cout << "Enter filename:";
  std::cin >> filename;
  const char CONTENT[] =
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

  PrintCharsToFile(&filename, CONTENT, sizeof(CONTENT));

  auto *readed_string = ReadStringFromFile(filename);

  const char* STRING_TO_FOUND = "labore";
  CheckForSubstring(readed_string, STRING_TO_FOUND);

  const char* STRING_TO_NOT_FOUND = "1234567";
  CheckForSubstring(readed_string, STRING_TO_NOT_FOUND);
  delete[] readed_string;
}
/* #endregion */

int main() {
  srand(time(nullptr));
  {
    /**
     * Task 1
     * Выделить в памяти динамический одномерный массив
     * типа int. Размер массива запросить у пользователя.
     * Инициализировать его числами степенями двойки: 1, 2, 4,
     * 8, 16, 32, 64, 128 ... Вывести массив на экран. Не забыть
     * освободить память. =) Разбить программу на функции.
     */
    Task1();
  }
  PrintSeparator();

  {
    /**
     * Task 2
     * Динамически выделить матрицу 4х4 типа int.
     * Инициализировать ее псевдослучайными числами через
     * функцию rand. Вывести на экран. Разбейте вашу
     * программу на функции которые вызываются из main.
     */
    Task2();
  }
  PrintSeparator();

  /**
   * Task 3
   * Написать программу, которая создаст два текстовых
   * файла (*.txt), примерно по 50-100 символов в каждом
   * (особого значения не имеет с каким именно содержимым).
   * Имена файлов запросить у польлзователя.
   */
  std::string filename1, filename2;
  {
    Task3(&filename1, &filename2);
  }
  PrintSeparator();

  {
    /**
     * Task 4
     * Написать функцию, «склеивающую» эти файлы в
     * третий текстовый файл (имя файлов спросить у
     * пользователя).
     */
    Task4(&filename1, &filename2);
  }
  PrintSeparator();

  {
    /**
     * Task 5
     * Написать программу, которая проверяет присутствует
     * ли указанное пользователем при запуске программы слово
     * в указанном пользователем файле (для простоты
     * работаем только с латиницей). Используем функцию find
     * которая есть в строках std::string.
     */
    Task5();
  }
  return 0;
}
